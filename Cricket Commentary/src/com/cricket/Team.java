package com.cricket;
import java.util.ArrayList;
import java.util.List;

public class Team 
{
	private String name;
	private List<Player> players = new ArrayList<Player>();
	private boolean isWon;
	private int nextPlayer = 0;
	private int score;
	private int ballsPlayed;
	private int noOfWicketsLost;
	private int totalWickets;
	
	public int getTotalWickets() {
		return totalWickets;
	}
	public void setTotalWickets(int totalWickets) {
		this.totalWickets = totalWickets;
	}
	public int getNoOfWicketsLost() {
		return noOfWicketsLost;
	}
	public void setNoOfWicketsLost(int noOfWicketsLost) {
		this.noOfWicketsLost = noOfWicketsLost;
	}
	public void addNoOfWicketsLost()
	{
		this.noOfWicketsLost++;
	}
	public int getBallsPlayed() {
		return ballsPlayed;
	}
	public void setBallsPlayed(int ballsPlayed) {
		this.ballsPlayed = ballsPlayed;
	}
	public void addBallsPlayed()
	{
		this.ballsPlayed++;
	}
	public int getScore() {
		return score;
	}
	public void setScore(int score) {
		this.score = score;
	}
	public Team() {
		super();
	}
	public Team(String name) {
		super();
		this.name = name;
	}
	public Team(String name, List<Player> players) {
		super();
		this.name = name;
		this.totalWickets=players.size();
		this.players = players;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<Player> getPlayers() {
		return players;
	}
	public void setPlayers(List<Player> players) {
		this.totalWickets = players.size();
		this.players = players;
	}
	public boolean isWon() {
		return isWon;
	}
	public void setWon(boolean isWon) {
		this.isWon = isWon;
	}
	public void addPlayer(Player player)
	{
		this.totalWickets++;
		this.players.add(player);
	}
	public Player getPlayer(int index)
	{
		return this.players.get(index);
	}
	public Player getNextPlayer()
	{
		if((this.getPlayers().size())>nextPlayer)
			return this.players.get(nextPlayer++);
		return null;
	}
	public void addScore(int score)
	{
		this.score+=score;
	}
	public void printScoreBoard()
	{
		System.out.println("\n*************"+this.name+" Score Board*****************\n");
		for(Player p : players)
		{
			if(p.isPlayed())
				System.out.println(p.printScore());
		}
		System.out.println("\n**********************END****************************\n");
	}
}
