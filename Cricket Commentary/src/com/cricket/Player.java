package com.cricket;

public class Player {
 private String name;
 private double list[] = new double[7];
 private int score;
 private int ballsFaced;
 private boolean isPlayed;
 private int noOfFours;
 private int noOfSixes;
 public void addFours()
 {
	 this.noOfFours++;
 }
 public void addSixes()
 {
	 this.noOfSixes++;
 }
 public int getNoOfFours() {
	return noOfFours;
}
public void setNoOfFours(int noOfFours) {
	this.noOfFours = noOfFours;
}
public int getNoOfSixes() {
	return noOfSixes;
}
public void setNoOfSixes(int noOfSixes) {
	this.noOfSixes = noOfSixes;
}
public boolean isPlayed() {
	return isPlayed;
}
public void setPlayed(boolean isPlayed) {
	this.isPlayed = isPlayed;
}
private boolean isOut;
 
 public boolean isOut() {
	return isOut;
}
public void setOut(boolean isOut) {
	this.isOut = isOut;
}
public int getScore() {
	return score;
}
public void setScore(int score) {
	this.score = score;
}
public int getBallsFaced() {
	return ballsFaced;
}
public void setBallsFaced(int ballsFaced) {
	this.ballsFaced = ballsFaced;
}
public Player()
 {
	 
 }
public Player(String name, double[] list) {
	super();
	this.name = name;
	this.list = list;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public double[] getList() {
	return list;
}
public void setList(double[] list) {
	this.list = list;
}
public void addScore(int score)
{
	this.score+=score;
}
public void addBalls()
{
	this.ballsFaced++;
}
public String printScore()
{
	return (this.name+(this.isOut()?"":"*")+" - "+this.score+"("+this.ballsFaced+" balls,"+this.noOfFours+" Fours,"+noOfSixes+" Sixes)" );
}
}
