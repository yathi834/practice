package com.cricket;

public class Problem1 {
	public final static  int  RUN_TO_SCORE = 40;
	public final static int OVERS_REMAINING = 4;
	public final static int WIKT_REMAINING = 3;
	/**
	 * Set up the weighed probability of players
	 * @param teamLengaburu
	 */
	public static void setUpTeam(Team teamLengaburu )
	{
		double[] list1 = {0.05,0.3,0.25,0.10,0.15,0.01,0.09,0.05};
		teamLengaburu.addPlayer(new Player("Kirat Boli",list1));
		double[] list2 = {0.1,0.4,0.2,0.05,0.10,0.01,0.04,0.1};
		teamLengaburu.addPlayer(new Player("NS Nodhi",list2));
		double[] list3 = {0.2,0.3,0.15,0.05,0.05,0.01,0.04,0.2};
		teamLengaburu.addPlayer(new Player("R Rumrah",list3));
		double[] list4 = {0.30,0.25,0.05,0.0,0.05,0.01,0.04,0.3};
		teamLengaburu.addPlayer(new Player("Shashi Henra",list4));
	}
	public static void main(String[] args) 
	{
		Team teamLengaburu = new Team("Lengaburu");
		setUpTeam(teamLengaburu);
		PlayMatch.doBatting(false, teamLengaburu, RUN_TO_SCORE, OVERS_REMAINING, WIKT_REMAINING);
		System.out.println("\nTotal score :"+teamLengaburu.getScore()+"("+teamLengaburu.getBallsPlayed()/6+"."+teamLengaburu.getBallsPlayed()%6+" overs)");
		if(teamLengaburu.isWon())
		{
			System.out.println("\n"+teamLengaburu.getName()+" won by "
					+(WIKT_REMAINING-teamLengaburu.getNoOfWicketsLost()) +" wicket and "
					+ (OVERS_REMAINING*6-teamLengaburu.getBallsPlayed()) +" balls remaining\n");
		}else
		{
			System.out.println("\n"+teamLengaburu.getName()+" lost by "
					+(RUN_TO_SCORE-teamLengaburu.getScore())+(((RUN_TO_SCORE-teamLengaburu.getScore())>1?" runs":" run"))+"\n");
		}
		
		teamLengaburu.printScoreBoard();
		
	}

}
