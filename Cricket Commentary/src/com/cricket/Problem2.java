package com.cricket;

public class Problem2 
{
	public final static int WIKT_REMAINING = 3;
	public final static int OVERS_REMAINING = 20;
	/**
	 * 0- DOT and 7 for OUT
	 * @param list
	 * @return
	 */
	
	private static void setUpTeams(Team teamLengaburu,Team teamEnchai)
	{
		double[] list1 = {0.05,0.3,0.25,0.10,0.15,0.01,0.09,0.05};
		teamLengaburu.addPlayer(new Player("Kirat Boli",list1));
		double[] list2 = {0.1,0.4,0.2,0.05,0.10,0.01,0.04,0.1};
		teamLengaburu.addPlayer(new Player("NS Nodhi",list2));
		double[] list3 = {0.2,0.3,0.15,0.05,0.05,0.01,0.04,0.2};
		teamLengaburu.addPlayer(new Player("R Rumrah",list3));
		double[] list4 = {0.30,0.25,0.05,0.0,0.05,0.01,0.04,0.3};
		teamLengaburu.addPlayer(new Player("Shashi Henra",list4));
		
		double[] list5 = {0.05,0.10,0.25,0.10,0.25,0.01,0.14,0.10};
		teamEnchai.addPlayer(new Player("DB Vellyers",list5));
		double[] list6 = {0.10,0.15,0.15,0.10,0.20,0.01,0.19,0.10};
		teamEnchai.addPlayer(new Player("H Mamla",list6));
		double[] list7 = {0.05,0.10,0.25,0.10,0.25,0.01,0.14,0.10};
		teamEnchai.addPlayer(new Player("Waid miths",list7));
		double[] list8 = {0.10,0.15,0.15,0.10,0.20,0.01,0.19,0.10};
		teamEnchai.addPlayer(new Player("Bravo",list8));
	}
	public static void main(String[] args) 
	{
		Team teamLengaburu = new Team("Lengaburu");
		Team teamEnchai = new Team("Enchai");
		setUpTeams(teamLengaburu, teamEnchai);
		System.out.println("Lengaburu innings");
		PlayMatch.doBatting(true, teamLengaburu, -1, OVERS_REMAINING, WIKT_REMAINING);
		System.out.println("\nLengaburu innings Score : "+teamLengaburu.getScore());
		System.out.println("Target "+(teamLengaburu.getScore()+1)+" runs in "+(OVERS_REMAINING)+" overs");
		System.out.println("\nEnchai innings");
		PlayMatch.doBatting(false, teamEnchai, teamLengaburu.getScore()+1, OVERS_REMAINING, WIKT_REMAINING);
		if(teamEnchai.isWon())
		{
			if((OVERS_REMAINING*6-teamEnchai.getBallsPlayed())>0)
				System.out.println("\nEnchai won with "+(OVERS_REMAINING*6-teamEnchai.getBallsPlayed())+" balls remaining\n");
			else
				System.out.println("\nEnchai won");
		}
		else
		{
			System.out.println("\nLengaburu won the match\n");
			
		}
		teamEnchai.printScoreBoard();
		teamLengaburu.printScoreBoard();	
	}

}
