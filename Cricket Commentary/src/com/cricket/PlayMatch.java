package com.cricket;

public class PlayMatch {
	private static int findScore(double[] list) {
		double random = Math.random();
		int temp = -1;
		for (int i = 0; i < list.length; i++) {
			random -= list[i];
			if (random < 0) {
				temp = i;
				break;
			}
		}
		return temp;
	}

	public static void doBatting(final boolean firstBatting,Team team,final int RUN_TO_SCORE, final int OVERS_REMAINING,final  int WIKT_REMAINING) 
	{
		int balls = 0;
		int over = 0;
		team.setNoOfWicketsLost(0);
		Player player1 = team.getNextPlayer();
		player1.setPlayed(true);
		Player player2 = team.getNextPlayer();
		player2.setPlayed(true);
		while (over < OVERS_REMAINING) 
		{
			balls++;
			team.addBallsPlayed();
			int run = findScore(player1.getList());
			if (run == 7) // Player1 is out
			{
				player1.addBalls(); // Set up his personal stats
				player1.setOut(true);
				System.out.print(over + "." + ((balls % 7)) + " " + player1.getName() + " out ");
				team.addNoOfWicketsLost();
				if (team.getNoOfWicketsLost() == WIKT_REMAINING) // If all are out break the loop set the flag
				{
					System.out.println("\n" + team.getName() + " All out!!!!");
					if(!firstBatting)
						team.setWon(false);
					break;
				}
				player1 = team.getNextPlayer();
				player1.setPlayed(true);	
				System.out.println(player1.getName() + " on field.");
			} 
			else 
			{
				System.out.println(over + "." + balls + " " + player1.getName()
				+ ((run == 0) ? " no run" : (" scores " + run + " run" + ((run > 1) ? "s" : ""))));
				player1.addScore(run);
				player1.addBalls();
				if (run == 4)
					player1.addFours();
				if (run == 6)
					player1.addSixes();
				if ((run != 0) && (run % 2 != 0)) // IF players takes odd runs swap them
				{
					Player tempVar;
					tempVar = player1;
					player1 = player2;
					player2 = tempVar;
				}
				team.addScore(run);
				if ( !firstBatting && team.getScore() >= RUN_TO_SCORE) // If score is more than target set flag and break loop
				{
					team.setWon(true);
					break;
				}
			}
			if (balls == 6) // Keep track of player swapping for every over
			{
				over++;
				balls = 0;
				if (over < OVERS_REMAINING)
				{
					if(!firstBatting)
						System.out.println("\n" + (OVERS_REMAINING - over) + " overs left. " + (RUN_TO_SCORE - team.getScore())
								+ " runs to win\n");
					else
						System.out.println("\n" + over + " overs " + team.getScore()+" runs\n");
				}
				Player tempVar;
				tempVar = player1;
				player1 = player2;
				player2 = tempVar;
			}
			}
		}
	
}
