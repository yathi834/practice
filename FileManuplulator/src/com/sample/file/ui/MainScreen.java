/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sample.file.ui;

import com.sample.file.controller.CustomOutputStream;
import com.sample.file.controller.FileTypeFilter;
import com.sample.file.controller.SimilarFileManupulator;
import static com.sample.file.controller.SimilarFileManupulator.progressCount;
import java.io.File;
import java.io.FilenameFilter;
import java.io.PrintStream;
import java.nio.file.Files;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JTextArea;
import javax.swing.ProgressMonitor;
import javax.swing.colorchooser.AbstractColorChooserPanel;

/**
 * @author uyat
 */
public class MainScreen extends javax.swing.JFrame {

    /**
     * Creates new form MainSc *
reen
     */
    private File srcDir = null;
    private File destFile = null;
    private FilenameFilter filter;
     
    public MainScreen() 
    {
////       // progress.get
       // System.setOut(printStream);
        jButton1 = new JButton();
        jComboBox2 = new JComboBox();
        customPanel = new JPanel();
        progressBar = new JProgressBar();
        initComponents();
        customPanel.setVisible(false);
        progressBar.setVisible(false);
        progressBar.setMaximum(100);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jTextField1 = new javax.swing.JTextField();
        jTextField2 = new javax.swing.JTextField();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jComboBox2 = new javax.swing.JComboBox<>();
        jCheckBox1 = new javax.swing.JCheckBox();
        customPanel = new javax.swing.JPanel();
        mp3CheckBox = new javax.swing.JCheckBox();
        mp4CheckBox = new javax.swing.JCheckBox();
        pdfCheckBox = new javax.swing.JCheckBox();
        docCheckBox = new javax.swing.JCheckBox();
        pngCheckBox = new javax.swing.JCheckBox();
        jpgCheckBox = new javax.swing.JCheckBox();
        jpegCheckBox = new javax.swing.JCheckBox();
        zipCheckBox = new javax.swing.JCheckBox();
        progressBar = new javax.swing.JProgressBar();
        jLabel2 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setFocusCycleRoot(false);

        jButton1.setText("Source");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setText("Destination");
        jButton2.setActionCommand("destBtn");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jTextField1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField1ActionPerformed(evt);
            }
        });

        jButton3.setText("Start");
        jButton3.setToolTipText("Start the process");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        jButton4.setText("Stop");
        jButton4.setToolTipText("stop the process");

        jLabel1.setText("Type");
        jLabel1.setToolTipText("");

        jComboBox2.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Audio", "Image", "Video", "Docs", "Custom" }));
        jComboBox2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox2ActionPerformed(evt);
            }
        });

        jCheckBox1.setText("Keep original");
        jCheckBox1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBox1ActionPerformed(evt);
            }
        });

        mp3CheckBox.setLabel("Mp3");

        mp4CheckBox.setText("Mp4");

        pdfCheckBox.setText("PDF");

        docCheckBox.setText("Docx");
        docCheckBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                docCheckBoxActionPerformed(evt);
            }
        });

        pngCheckBox.setText("PNG");

        jpgCheckBox.setText("JPG");

        jpegCheckBox.setText("JPEG");

        zipCheckBox.setText("Zip");

        javax.swing.GroupLayout customPanelLayout = new javax.swing.GroupLayout(customPanel);
        customPanel.setLayout(customPanelLayout);
        customPanelLayout.setHorizontalGroup(
            customPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(customPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(customPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(customPanelLayout.createSequentialGroup()
                        .addComponent(mp3CheckBox)
                        .addGap(18, 18, 18)
                        .addComponent(mp4CheckBox))
                    .addGroup(customPanelLayout.createSequentialGroup()
                        .addGroup(customPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(pdfCheckBox)
                            .addComponent(jpegCheckBox))
                        .addGap(18, 18, 18)
                        .addGroup(customPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(docCheckBox)
                            .addComponent(jpgCheckBox))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(customPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(pngCheckBox)
                            .addComponent(zipCheckBox))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        customPanelLayout.setVerticalGroup(
            customPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(customPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(customPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(mp3CheckBox)
                    .addComponent(mp4CheckBox))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(customPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(pdfCheckBox)
                    .addComponent(docCheckBox)
                    .addComponent(zipCheckBox))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(customPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jpegCheckBox)
                    .addComponent(jpgCheckBox)
                    .addComponent(pngCheckBox))
                .addContainerGap(18, Short.MAX_VALUE))
        );

        jLabel2.setText("Progress..");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(101, 101, 101)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(customPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jButton2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jButton1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(99, 99, 99)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jCheckBox1)
                            .addComponent(jTextField1)
                            .addComponent(jTextField2)
                            .addComponent(jComboBox2, 0, 243, Short.MAX_VALUE))))
                .addContainerGap(139, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(89, 89, 89)
                .addComponent(jLabel2)
                .addGap(38, 38, 38)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(progressBar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jButton3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButton4)))
                .addGap(184, 184, 184))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(56, 56, 56)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton1)
                    .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jCheckBox1)
                .addGap(10, 10, 10)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton2)
                    .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(48, 48, 48)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addComponent(jComboBox2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(33, 33, 33)
                .addComponent(customPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jButton3)
                            .addComponent(jButton4))
                        .addGap(26, 26, 26)
                        .addComponent(progressBar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.TRAILING))
                .addGap(31, 31, 31))
        );

        jCheckBox1.getAccessibleContext().setAccessibleName("keepOriginal");

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
                // TODO add your handling code here:
        JFileChooser chooser = new JFileChooser();
        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
    // disable the "All files" option.
        chooser.setAcceptAllFileFilterUsed(false);
        // optionally set chooser options ...
        if (chooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
            
            jTextField1.setText(srcDir.getAbsolutePath());
            
        } else {
           
        }
    
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        // TODO add your handling code here:
        //JOptionPane confirm = new JOptionPane("Are you ready to proceed?",JOptionPane.QUESTION_MESSAGE,JOptionPane.YES_NO_OPTION);
        if(!jTextField2.getText().equals(""))
        {
             srcDir = new File(jTextField1.getText());
             if(!srcDir.exists())
             {
                 JOptionPane.showMessageDialog(null,"Source folder doesn't exists","Error",JOptionPane.ERROR_MESSAGE);
                 return;
             }
             
        }
        else
        {
            JOptionPane.showMessageDialog(null,"Source can't null","Error",JOptionPane.ERROR_MESSAGE);
            return;
        }
        
       
        if(!jTextField2.getText().equals(""))
        {
            destFile = new File(jTextField2.getText());
             if(!destFile.exists())
             {
                 JOptionPane.showMessageDialog(null,"Destination folder doesn't exists","Error",JOptionPane.ERROR_MESSAGE);
                 return;
             }
             
        }
        else
        {
            JOptionPane.showMessageDialog(null,"Provide destination","Error",JOptionPane.ERROR_MESSAGE);
            return;
        }

        if(customPanel.isVisible())
        {
            ArrayList<String> list = new ArrayList();
            if(mp3CheckBox.isSelected())                
                list.add("mp3");
            if(mp4CheckBox.isSelected())
                list.add("mp4");
            if(pdfCheckBox.isSelected())
                list.add("pdf");
            if(pngCheckBox.isSelected())
                list.add("png");
            if(zipCheckBox.isSelected())
                list.add("zip");
            if(jpgCheckBox.isSelected())
                list.add("jpg");
            if(jpegCheckBox.isSelected())
                list.add("jpeg");
            if(docCheckBox.isSelected())
                list.add("docx");
            filter = FileTypeFilter.customFilter(list);
        }
        progressBar.setVisible(true);
        progressBar.setValue(progressCount);
        progressBar.setFocusable(true);
        int res = JOptionPane.showConfirmDialog(null,"Confirm","Confirm?",JOptionPane.YES_NO_OPTION);
        if(res == JOptionPane.YES_OPTION)
        {
           
           SimilarFileManupulator s = new SimilarFileManupulator();
           ProgressMonitor monitor = new ProgressMonitor(null,"Progress","progress",0, 100);
           if(s.manageFiles(srcDir.getAbsolutePath(),destFile.getAbsolutePath(), filter,jCheckBox1.isSelected(),progressBar))
            {
             JOptionPane.showMessageDialog(null, "Success", "InfoBox: ", JOptionPane.INFORMATION_MESSAGE);
            } 
           //while()
        }
        
        
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jComboBox2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox2ActionPerformed
        // TODO add your handling code here:
        int selected =jComboBox2.getSelectedIndex();
        if(customPanel.isVisible()) customPanel.setVisible(false);
        switch(selected)
        {
            case 0 : filter = FileTypeFilter.AUDIO_FILTER;break;
            case 1 : filter = FileTypeFilter.IMAGE_FILTER;break;
            case 2 : filter = FileTypeFilter.VIDEO_FILTER;break;
            case 3 : filter = FileTypeFilter.DOC_FILTER;break;
            case 4 : customPanel.setVisible(true);
        }
    }//GEN-LAST:event_jComboBox2ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
         JFileChooser chooser = new JFileChooser();
         chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
    //
    // disable the "All files" option.
    //
        chooser.setAcceptAllFileFilterUsed(false);
         //chooser.f
        // optionally set chooser options ...
        if (chooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
            jTextField2.setText(destFile.getAbsolutePath());
            
        } else {
           
        }
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jTextField1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField1ActionPerformed

    private void jCheckBox1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBox1ActionPerformed
        // TODO add your handling code here:
        
    }//GEN-LAST:event_jCheckBox1ActionPerformed

    private void docCheckBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_docCheckBoxActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_docCheckBoxActionPerformed

    /**
     * @param args the command line arguments
    */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainScreen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainScreen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainScreen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainScreen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MainScreen().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel customPanel;
    private javax.swing.JCheckBox docCheckBox;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JCheckBox jCheckBox1;
    private javax.swing.JComboBox<String> jComboBox2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTextField jTextField2;
    private javax.swing.JCheckBox jpegCheckBox;
    private javax.swing.JCheckBox jpgCheckBox;
    private javax.swing.JCheckBox mp3CheckBox;
    private javax.swing.JCheckBox mp4CheckBox;
    private javax.swing.JCheckBox pdfCheckBox;
    private javax.swing.JCheckBox pngCheckBox;
    private javax.swing.JProgressBar progressBar;
    private javax.swing.JCheckBox zipCheckBox;
    // End of variables declaration//GEN-END:variables
}
