package com.sample.file.controller;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;

public class FileTypeFilter{
	
	public static final String[] IMAGE_EXTENSIONS = new String[]{
	        "gif", "png", "bmp","jpeg","jpg" 
	    };
	public static final String[] AUDIO_EXTENSIONS = new String[]{
		"mp3","wav"	
	};
	public static final String[] VIDEO_EXTENSIONS = new String[]{
			"mp4","mkv"	
		};
	public static final String[] DOC_EXTENSIONS = new String[]{
			"doc","txt","pdf","docx"
		};
	
	public static final FilenameFilter IMAGE_FILTER = (File dir, String name)->{
		for(String temp:IMAGE_EXTENSIONS)
			if(name.endsWith("."+temp))
				return true;
		return false;
	};
	public static final FilenameFilter AUDIO_FILTER = (File dir, String name)->{
		for(String temp:AUDIO_EXTENSIONS)
			if(name.endsWith("."+temp))
				return true;
		return false;
	};
	public static final FilenameFilter VIDEO_FILTER = (File dir, String name)->{
		for(String temp:VIDEO_EXTENSIONS)
			if(name.endsWith("."+temp))
				return true;
		return false;
	};	
	public static final FilenameFilter DIR_FILTER = (File dir, String name)->{
		File temp = new File(dir,name);
		if(temp.isDirectory())
			return true;
		return false;
	};
	public static final FilenameFilter DOC_FILTER = (File dir, String name)->{
		for(String temp:DOC_EXTENSIONS)
			if(name.endsWith("."+temp))
				return true;
		return false;
	};
	
	public static FilenameFilter customFilter(ArrayList<String> type)
	{
		FilenameFilter CUSTOM_FILTER = (File dir, String name)->{
			for(String temp:type)
				if(name.endsWith("."+temp))
					return true;
			return false;
		};
		return CUSTOM_FILTER;
	}
}
