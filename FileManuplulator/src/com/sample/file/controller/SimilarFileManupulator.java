package com.sample.file.controller;

import java.io.File;
import java.io.FilenameFilter;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import javax.swing.JProgressBar;

public class SimilarFileManupulator {
        public static int progressCount=0;
        public synchronized static void incrementProgress(int value)
        {
            progressCount+=value;
           // progress.setValue(progressCount);
        }
	public List<File> getSimilarFile(String dir,FilenameFilter filter){
		List<File> fList = null;
		List<File> dList = null;
		File directory = new File(dir);
		if(directory.isDirectory())
		{
			fList = new ArrayList<>(Arrays.asList(directory.listFiles(filter)));
			dList = new ArrayList<>(Arrays.asList(directory.listFiles(FileTypeFilter.DIR_FILTER)));
			/*for(File temp:dList)
				fList.addAll(getSimilarFile(temp.getPath(), filter));*/
		}
		return  fList;

	}
//        public boolean manageFiles(File srcDir,File destDir,FilenameFilter filter,boolean keepOriginal)
//        {
//            List<File> list = getSimilarFile(srcDir.getAbsolutePath(), filter);
//            ExecutorService executor = Executors.newFixedThreadPool(5);
//		for(File temp:list)
//		{
//
//                  executor.execute(()->{
//                      try {
//				System.out.println("File "+temp.getAbsolutePath()+": Copying.......");
//                                incrementProgress(100/list.size(),progress);
//				Files.copy(temp.toPath(), destDir.toPath());
//			} catch (Exception e) {
//				System.out.println("Exception Occured During copy of:  "+temp.getAbsolutePath()+" : "+e.getMessage());
//			}
//			if(!keepOriginal)
//				temp.delete();
//			});
//			
//		}
//                executor.shutdown();
//                try {
//                     executor.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
//                    } catch (InterruptedException e) {
//                        }
//                while(!executor.isShutdown());
//              
//                 return true;
//        }

        @SuppressWarnings("empty-statement")
	public boolean manageFiles(String srcDir,String destDir,FilenameFilter filter,boolean keepOriginal,JProgressBar progress)
	{
		File dest = new File(destDir);
		if(!dest.exists())
			dest.mkdirs();
		File src = new File(srcDir);
		if(!src.exists())
			throw new IllegalArgumentException("Directory doesn't exists");
		List<File> list = getSimilarFile(srcDir, filter);
                ExecutorService executor = Executors.newFixedThreadPool(5);
		for(File temp:list)
		{
                   
                    executor.execute(()->{
                      try {
				System.out.println("File "+temp.getAbsolutePath()+": Copying.......");
                                incrementProgress(100/list.size());
                              //  progress.setText(progress.getText()+"\nFile :"+temp.getAbsolutePath()+": Copying.......");
				Files.copy(temp.toPath(), new File(dest,temp.getName()).toPath());
			} catch (Exception e) {
				System.out.println("Exception Occured During copy of:  "+temp.getAbsolutePath()+" : "+e.getMessage());
			}
			if(!keepOriginal)
				temp.delete();
			});
			
		}
                executor.shutdown();
                
                try {
                     executor.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
                    } catch (InterruptedException e) {
                        }
                while(!executor.isShutdown());
              
                 return true;	
	}
	
}
